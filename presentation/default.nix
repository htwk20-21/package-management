with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "texliveBeamer";
  env = buildEnv { name = name; paths = buildInputs; };

  latex = pkgs.texlive.combine {
               inherit (pkgs.texlive)
               scheme-full
               beamer;
           };

  buildInputs = [
    latex
  ];
}
