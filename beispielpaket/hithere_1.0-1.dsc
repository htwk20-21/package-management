Format: 3.0 (quilt)
Source: hithere
Binary: hithere
Architecture: any
Version: 1.0-1
Maintainer: Peter Hornik <peter.hornik@stud.htwk-leipzig.de>
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 9)
Package-List:
 hithere deb misc optional arch=any
Checksums-Sha1:
 2dcd65497a12a3ea03223f52186447bd5733dce9 617 hithere_1.0.orig.tar.gz
 e8185ae0d3a31cbf795078affce913662bdaee93 744 hithere_1.0-1.debian.tar.xz
Checksums-Sha256:
 63062b582a712f169f37a5f52a41aa3ca9a405aafb8aa837bc906fa413b62cdb 617 hithere_1.0.orig.tar.gz
 775b84869981a80749dfd8d0704552335baa67c3339f016f4658ef73bdf9a07a 744 hithere_1.0-1.debian.tar.xz
Files:
 5b2830fa1fcd44ce489774771625526e 617 hithere_1.0.orig.tar.gz
 8b09c469bc3985aec6856d0f8aad9501 744 hithere_1.0-1.debian.tar.xz
